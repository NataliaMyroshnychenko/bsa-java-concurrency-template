package bsa.java.concurrency.fs;

import bsa.java.concurrency.image.ImageRepository;
import bsa.java.concurrency.image.entity.Image;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class FileService {

    @Autowired
    private ImageRepository imageRepository;

    private final String pathToUpload = "E:/Binary/4_Concurrency/uploads/";

    @SneakyThrows
    @Async
    public CompletableFuture<String> saveFile(MultipartFile file) {

        checkDirectoryForUpload(pathToUpload);

        String pathToSaveFile = pathToUpload + file.getResource().getFilename();
        File savedFile = new File(pathToSaveFile);

        file.transferTo(savedFile);

        return CompletableFuture.completedFuture(pathToSaveFile);
    }

    private void checkDirectoryForUpload(String path) {
        if (!new File(path).exists()) {
            new File(path).mkdir();
        }
    }

    public boolean deleteById(UUID imageId) {
        Optional<Image> image = imageRepository.findById(imageId);

        if (image.isEmpty()) return false;

        String path = image.get().getPath();
        File file = new File(path);
        boolean isDeletedFile = file.delete();

        return isDeletedFile;
    }

    public void deleteAllFilesInFileSystem() {
        for (File myFile : Objects.requireNonNull(new File(pathToUpload).listFiles())) {
            if (myFile.isFile()) myFile.delete();
        }
    }

    public File uploadToFile(MultipartFile file) {
        File uploadingFile = null;
        CompletableFuture<String> future = saveFile(file);
        try {
            uploadingFile = new File(future.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return uploadingFile;
    }
}

