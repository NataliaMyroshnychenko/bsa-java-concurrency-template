package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileService;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import bsa.java.concurrency.util.DHasher;
import bsa.java.concurrency.image.service.ImageService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private FileService fileService;

    @Autowired
    private ImageService imageService;

    private static final Logger log = getLogger(ImageController.class);

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        log.info("--- start method batchUploadImages with param {}", Arrays.stream(files).map(MultipartFile::getName).collect(Collectors.toList()));

        List<CompletableFuture<String>> completablePathImages = new ArrayList<>();
        for (MultipartFile file : files) {
            CompletableFuture<String> path = fileService.saveFile(file);
            completablePathImages.add(path);
            log.info("--- method batchUploadImages save file {} ", file.getResource().getFilename());
        }

        List<CompletableFuture<Image>> completableImages = completablePathImages.stream()
                .map(pathFuture -> CompletableFuture.supplyAsync(() -> imageService.createImage(pathFuture)))
                .collect(Collectors.toList());

        List<Image> notNullImages = completableImages
                .stream()
                .map(CompletableFuture::join)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        log.info("--- method batchUploadImages save to database these images {}", notNullImages);

        imageService.saveAll(notNullImages);
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
                                               @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {

        File inputFile = fileService.uploadToFile(file);

        if (inputFile == null) return new ArrayList<>();

        long hashByFile = DHasher.getHashByFile(inputFile);

        List<SearchResultDTO> matcherListByHash = imageService.searchOfMatchersInDataBase(hashByFile);
        List<SearchResultDTO> matcherListByThreshold = matcherListByHash.stream()
                .filter(fileSearchResult ->
                        (fileSearchResult.getMatchPercent() >= threshold))
                .collect(Collectors.toList());

        if (matcherListByThreshold.isEmpty()) {
            CompletableFuture<String> completableFuture = fileService.saveFile(file);
            imageService.createImage(completableFuture);
        } else {
            inputFile.delete();
        }
        return matcherListByThreshold;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        log.info("--- start method delete image by id = {}", imageId);
        if (imageId == null) return;

        boolean isDeletedInFS = fileService.deleteById(imageId);
        log.info("--- {} image in FS by id = {}", (isDeletedInFS ? "Is" : "Is not"), imageId);

        if (isDeletedInFS) {
            imageService.deleteById(imageId);
        }
        boolean isDeletedInDB = !imageService.findById(imageId);
        log.info("--- {} image in DB by id = {}", (isDeletedInDB ? "Is" : "Is not"), imageId);

    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages() {
        log.info("--- start method deleteAll");
        fileService.deleteAllFilesInFileSystem();
        imageService.deleteAllFilesInDataBase();
    }


}
