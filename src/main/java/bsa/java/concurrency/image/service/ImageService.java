package bsa.java.concurrency.image.service;

import bsa.java.concurrency.image.ImageRepository;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import bsa.java.concurrency.util.DHasher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    public void deleteById(UUID imageId) {
        if (imageId == null) return;
        imageRepository.deleteById(imageId);
    }

    public Optional<Image> getById(UUID imageId) {
        return imageRepository.findById(imageId);
    }

    public boolean findById(UUID imageId) {
        Optional<Image> image = getById(imageId);
        return image.isPresent();
    }

    public void deleteAllFilesInDataBase() {
        imageRepository.deleteAll();
    }

    public Image save(Image image) {
        return imageRepository.save(image);
    }

    public Image createImage(CompletableFuture<String> pathFuture) {
        Image image = null;
        try {
            String path = pathFuture.get();
            long hashByFile = DHasher.getHashByFile(new File(path));
            image = new Image(UUID.randomUUID(), hashByFile, path);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return image;
    }

    public void saveAll(List<Image> images) {
        for (Image image : images) {
            save(image);
        }
    }

    public List<SearchResultDTO> searchOfMatchersInDataBase(long inputHash) {
        List<Image> imageList = imageRepository.findAll();
        List<SearchResultDTO> searchResultDTO = imageList.stream().map(image -> {
            double matchPercent = DHasher.matcherHash(inputHash, image.getHash());

            return SearchResultDTO.getSearchResultDTO(image.getId(), matchPercent, image.getPath());
        }).collect(Collectors.toList());

        return searchResultDTO;
    }
}
