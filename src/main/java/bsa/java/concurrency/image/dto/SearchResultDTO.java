package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Builder
@AllArgsConstructor
@Setter
@Getter
public class SearchResultDTO {

    private UUID imageId;
    private Double matchPercent;
    private String imageUrl;

    public static SearchResultDTO getSearchResultDTO(UUID imageId, Double matchPercent, String imageUrl) {
        return SearchResultDTO.builder()
                .imageId(imageId)
                .matchPercent(matchPercent)
                .imageUrl(imageUrl)
                .build();
    }
}
