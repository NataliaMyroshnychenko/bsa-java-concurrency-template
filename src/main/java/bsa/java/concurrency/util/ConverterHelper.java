package bsa.java.concurrency.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class ConverterHelper {

    public static byte[] fileToByteArray(File file) {

        BufferedImage bImage = null;
        byte[] data = new byte[0];
        try {
            bImage = ImageIO.read(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            String formatName = getFormatName(file.getName());
            ImageIO.write(bImage, formatName, bos);
            data = bos.toByteArray();

            return data;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    private static String getFormatName(String fileName) {
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }
}

